import Vue from 'vue'
import Vuex from 'vuex'
import Request from '@/Utils/Request'

Vue.use(Vuex)

const state = {
  msg: '',
  info: {},
  showDialogLoadingScreen: true,
  incentives: '',
  rawDatas: '',
  rawBQs: '',
  qualityList: '',
  traSoat: '',
  promotions: '',
  baoHiemList: '',
  traSoatInsertList: ''
}

const getters = {
  getInfo: (state) => state.info,
  getMsg: (state) => state.msg,
  getShowDialogLoadingScreen: (state) => state.showDialogLoadingScreen,
  getIncentives: (state) => state.incentives,
  getRawDatas: (state) => state.rawDatas,
  getRawBQs: (state) => state.rawBQs,
  getQualityList: (state) => state.qualityList,
  getTraSoat: (state) => state.traSoat,
  getPromotions: (state) => state.promotions,
  getBaoHiemList: (state) => state.baoHiemList,
  getTraSoatInsertList: (state) => state.traSoatInsertList
}

const actions = {
  updateMsg ({ commit }, msg) {
    commit('UPDATE_MSG', msg)
  },
  updateShowDialogLoadingScreen ({ commit }, status) {
    commit('UPDATE_SHOW_DIALOG_LOADING_SCREEN', status)
  },
  getInfo ({ commit }) {
    Request.postData('/user/info', {}, res => {
      if (res.status) {
        setTimeout(() => {
          commit('UPDATE_INFO', res.data)
          commit('UPDATE_SHOW_DIALOG_LOADING_SCREEN', false)
        }, 500)
      }
    })
  },
  login ({ commit }, data) {
    Request.postData('/user/login', data, res => {
      if (res.status) {
        let token = res.data.token
        localStorage.token = token
        window.location.href = '/'
      } else {
        let msg = res.msg
        commit('UPDATE_MSG', msg)
      }
    })
  },
  getIncentives ({ commit }) {
    Request.postData('/incentive/list', {}, res => {
      if (res.status) {
        commit('UPDATE_INCENTIVES', res.data)
      } else {
        let msg = res.msg
        commit('UPDATE_MSG', msg)
      }
    })
  },
  getRawDatas ({ commit }) {
    Request.postData('/raw-data/list', {}, res => {
      if (res.status) {
        commit('UPDATE_RAW_DATA', res.data)
      } else {
        let msg = res.msg
        commit('UPDATE_MSG', msg)
      }
    })
  },
  getRawBQs ({ commit }, data) {
    Request.postData('/raw-bq/list', data, res => {
      if (res.status) {
        commit('UPDATE_RAW_BQ', res.data)
      } else {
        let msg = res.msg
        commit('UPDATE_MSG', msg)
      }
    })
  },
  getQualityList ({ commit }, data) {
    Request.postData('/quality/list', data, res => {
      if (res.status) {
        commit('UPDATE_QUALITY_LIST', res.data)
      } else {
        let msg = res.msg
        commit('UPDATE_MSG', msg)
      }
    })
  },
  getTraSoat ({ commit }) {
    Request.postData('/tra-soat/one', {}, res => {
      if (res.status) {
        commit('UPDATE_TRA_SOAT', res.data)
      } else {
        let msg = res.msg
        commit('UPDATE_MSG', msg)
      }
    })
  },
  getPromotions ({ commit }, data) {
    Request.postData('/promotion/list', data, res => {
      if (res.status) {
        commit('UPDATE_PROMOTIONS', res.data)
      } else {
        let msg = res.msg
        commit('UPDATE_MSG', msg)
      }
    })
  },
  getBaoHiemList ({ commit }, data) {
    Request.postData('/bao-hiem/list', data, res => {
      if (res.status) {
        commit('UPDATE_BAO_HIEM', res.data)
      } else {
        let msg = res.msg
        commit('UPDATE_MSG', msg)
      }
    })
  },
  getTraSoatInsertList ({ commit }, data) {
    Request.postData('/tra-soat-insert/list', data, res => {
      if (res.status) {
        commit('UPDATE_TRA_SOAT_INSERT_LIST', res.data)
      } else {
        let msg = res.msg
        commit('UPDATE_MSG', msg)
      }
    })
  }
}

const mutations = {
  UPDATE_INFO (state, info) {
    state.info = info
  },
  UPDATE_MSG (state, msg) {
    state.msg = msg
  },
  UPDATE_SHOW_DIALOG_LOADING_SCREEN (state, status) {
    state.showDialogLoadingScreen = status
  },
  UPDATE_INCENTIVES (state, incentives) {
    state.incentives = incentives
  },
  UPDATE_RAW_DATA (state, rawDatas) {
    state.rawDatas = rawDatas
  },
  UPDATE_RAW_BQ (state, rawBQs) {
    state.rawBQs = rawBQs
  },
  UPDATE_QUALITY_LIST (state, quality) {
    state.qualityList = quality
  },
  UPDATE_TRA_SOAT (state, traSoat) {
    state.traSoat = traSoat
  },
  UPDATE_PROMOTIONS (state, promotions) {
    state.promotions = promotions
  },
  UPDATE_BAO_HIEM (state, baoHiem) {
    state.baoHiemList = baoHiem
  },
  UPDATE_TRA_SOAT_INSERT_LIST (state, list) {
    state.traSoatInsertList = list
  }
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  modules: {
    // wallet
  }
})
