import Vue from 'vue'
import Router from 'vue-router'
import Theme from '@/views/Theme'
import Login from '@/views/Login'
import Incentives from '@/views/Incentives'
import RawData from '@/views/RawData'
import RawBQ from '@/views/RawBQ'
import Quality from '@/views/Quality'
import TraSoat from '@/views/TraSoat'
import GuiTraSoat from '@/views/GuiTraSoat'
import Promotion from '@/views/Promotion'
import BaoHiem from '@/views/BaoHiem'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Theme',
      redirect: '/incentives',
      component: Theme,
      children: [
        {
          path: '/incentives',
          name: 'Incentives',
          component: Incentives,
          meta: {
            auth: true
          }
        },
        {
          path: '/rawdata',
          name: 'RawData',
          component: RawData,
          meta: {
            auth: true
          }
        },
        {
          path: '/rawbq',
          name: 'RawBQ',
          component: RawBQ,
          meta: {
            auth: true
          }
        },
        {
          path: '/quality',
          name: 'Quality',
          component: Quality,
          meta: {
            auth: true
          }
        },
        {
          path: '/tra-soat',
          name: 'TraSoat',
          component: TraSoat,
          meta: {
            auth: true
          }
        },
        {
          path: '/gui-tra-soat',
          name: 'GuiTraSoat',
          component: GuiTraSoat,
          meta: {
            auth: true
          }
        },
        {
          path: '/promotion',
          name: 'Promotion',
          component: Promotion,
          meta: {
            auth: true
          }
        },
        {
          path: '/bao-hiem',
          name: 'BaoHiem',
          component: BaoHiem,
          meta: {
            auth: true
          }
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: {
        render (c) { return c(Login) }
      },
      meta: {
        auth: false
      }
    }
  ]
})
