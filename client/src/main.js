import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import router from './router'
import store from './store/store'
import MLVuePlugin from './Utils/vue/MLVuePLugin'

import './authenticate'

import './assets/css/roboto-material-icon.css'
import './assets/css/normalize.css'
import 'vuetify/dist/vuetify.min.css'
import './assets/sass/style.sass'

Vue.use(MLVuePlugin)

Vue.use(Vuetify, {
  theme: {
    primary: '#008446',
    secondary: '#429ACE'
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
