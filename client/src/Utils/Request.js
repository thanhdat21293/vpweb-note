import axios from 'axios'
const SERVER = process.env.SERVER
const qs = require('qs')
/* eslint-disable */
class Request {
  constructor () {
		this.headers = {
			'Accept': 'application/json',
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	}

	getData(url, data, callback) {
		url = this.getUrl(url, data);
		this.fetchData('GET', url, data, callback);
	}

	postData (url, data, callback) {
		url = this.getUrl(url)
		this.fetchData('POST', url, data, callback)
  }

  fetchData(method, url, data, callback) {
		method = method.toLocaleUpperCase()
		let headers = this.headers
		let token = localStorage.token || ''
		switch (method) {
			case 'POST':
			case 'PUT':
				data = qs.stringify(data)
				break
		}
		headers['authorization'] = 'AuthJWT ' + token
		
		axios({
			method,
			url,
			data,
			headers
		})
			.then(res => {
				// console.log(res)
				if (res.data && res.data.error === 706) {
					if (localStorage.token && localStorage.token.length > 0) {
						localStorage.removeItem('token')
					}
					window.location.href = '/'
				} else {
					callback(res.data)
				}
			})
			.catch(err => {
				console.log(err)
				callback({status: false, msg: 'Có lỗi xảy ra'})
			})
	}

  serialize (obj) {
		let str = []
		for (let p in obj)
			if (obj.hasOwnProperty(p)) {
				str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
			}
		return str.join('&')
	}

	getUrl (url, data = '') {
		if (data) {
			return `${SERVER}/api${url}?${this.serialize(data)}`
    } else {
			return `${SERVER}/api${url}`
    }
	}
	
}

export default new Request()
