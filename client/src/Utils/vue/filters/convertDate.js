/**
 * dataTable
 */
import moment from 'moment'

export default (date) => {
  if (date) {
    return moment(date).format('DD/MM/YYYY')
  }
  return date
}
