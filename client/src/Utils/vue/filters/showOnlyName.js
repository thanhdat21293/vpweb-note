/**
 * showOnlyName
 */

export default (fullname) => {
  let arr = fullname.split(' ')
  arr = arr.filter((n) => { return n.length > 0 })
  let count = arr.length
  for (let i = 0; i < count; i++) {
    if (i < count - 1) {
      arr[i] = '-'
    }
  }
  let onlyName = arr.join(' ')
  return onlyName
}
