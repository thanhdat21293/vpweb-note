/**
 * monthYear
 */
import moment from 'moment'

export default (date) => {
  if (date) {
    return moment(date).format('MM/YYYY')
  }
  return date // 02/2018
}
