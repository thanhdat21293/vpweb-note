/**
 * hide2LastNumber
 */

export default (number) => {
  let arr = number.split('')
  let count = arr.length
  arr[count - 1] = 'x'
  arr[count - 2] = 'x'
  let numberHide = arr.join('')
  return numberHide
}
