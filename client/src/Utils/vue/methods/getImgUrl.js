/**
 * getImgUrl
 */
export default function (name) {
  let images = require.context('../../../assets/images', false)
  return images(`./${name}`)
}
