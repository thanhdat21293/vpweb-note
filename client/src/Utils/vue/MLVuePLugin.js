/**
 * Vue Plugin
 */
import * as filters from './filters'
import * as methods from './methods'

function install (Vue, options = {}) {
  if (process.env.NODE_ENV !== 'production') {
    console.info('[dev-log] MLVuePlugin options:', options)
  }

  // register global filter
  for (let key in filters) {
    Vue.filter(key, filters[key])
  }

  // register global method
  Vue.mixin({
    methods: {
      ...methods
    }
  })
}

export default install
