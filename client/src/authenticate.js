/**
 * Authenticate
 */
import router from './router'

router.beforeEach((to, from, next) => {
  if (to.meta.auth) {
    if (localStorage.token) {
      next()
    } else {
      window.location.href = '/login'
    }
  } else {
    next()
  }
})
