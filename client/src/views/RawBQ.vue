<template>
  <div>
    <h3 class="title">SAO KÊ HUY ĐỘNG ĐỂ KIỂM TRA CHỈ TIÊU BÌNH QUÂN TĂNG RÒNG</h3>
    <span class="subtitle"></span>
    <download-excel
      ref="exportData"
      :clickBtn="clickExport"
      :data   = "json_data"
      :fields = "json_fields"
      name    = "RawBQ.xls">
    </download-excel>
    <div v-if="loadingExport">
      <Loading :background="true" />
    </div>
    <div v-if="typeof rawBQs === 'object'" style="position: relative">
      <div style="text-align: right">
        <v-btn color="info" @click="getDataExport" >Export to Excel</v-btn>
      </div>
      <div>
        <div class="scroll-x">
          <table class="table">
            <thead>
              <tr>
                <th v-for="(key, index) in headers" :key="index">{{ key }}</th>
              </tr>
            </thead>
            <tbody>
              <tr v-if="bodies.length > 0" v-for="(incentive, index) in bodies" :key="index">
                <td v-for="(item, key) in incentive" :key="key">
                  <span v-if="key === 'cus_open_date' || key === 'openning_date' || key === 'issue_date' || key === 'matdt' || key === 'first_dist_date' || key === 'tcsign_date'">{{ item | convertDate }}</span>
                  <span v-else>{{ item }}</span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <br>
        <div class="text-xs-center">
          <v-pagination :length="total" v-model="currentPage"></v-pagination>
        </div>
      </div>
      <Loading v-if="loadingData" :background="true" />
    </div>
    <div v-else style="min-height: 100px; position: relative">
      <Loading />
    </div>
  </div>
</template>

<script>
import { mapGetters } from 'vuex'
import axios from 'axios'
import DownloadExcel from '@/components/ExportExcel'
import Loading from '@/components/layout/Loading'

let SERVER = process.env.SERVER

export default {
  name: 'RawBQ',
  components: {
    Loading,
    DownloadExcel
  },
  data () {
    return {
      headers: [],
      bodies: [],
      currentPage: 1,
      total: 0,
      loadingData: false,
      loadingExport: false,
      clickExport: false,
      json_fields: {},
      json_data: [],
      json_meta: [
        [{
          'key': 'charset',
          'value': 'utf-8'
        }]
      ]
    }
  },
  computed: {
    ...mapGetters({
      rawBQs: 'getRawBQs',
      msg: 'getMsg'
    })
  },
  mounted () {
    this.loadingData = true
    let data = {
      currentPage: this.currentPage
    }
    this.$store.dispatch('getRawBQs', data)
  },
  watch: {
    msg () {
      this.loadingData = false
    },
    rawBQs () {
      this.loadingData = false
      this.total = Math.ceil(this.rawBQs.total / 10)
      this.convertDataToTable(this.rawBQs.data)
    },
    currentPage () {
      this.loadingData = true
      let data = {
        currentPage: this.currentPage
      }
      this.$store.dispatch('getRawBQs', data)
    }
  },
  methods: {
    getDataExport () {
      this.loadingExport = true
      let token = localStorage.token
      axios.post(`${SERVER}/api/raw-bq/exports`, {}, {
        headers: {
          authorization: 'AuthJWT ' + token
        }
      })
        .then(res => {
          this.loadingExport = false
          let data = res.data.data
          if (res.data.status) {
            let listHeader = Object.keys(data.data[0])
            let objHeader = {}
            listHeader.map(item => {
              objHeader[item] = 'String'
            })
            this.json_fields = objHeader
            this.json_data = data.data
            this.clickExport = !this.clickExport
          } else {
            this.$store.dispatch('updateMsg', 'Có lỗi xảy ra')
          }
        })
        .catch(error => {
          this.loadingExport = false
          this.$store.dispatch('updateMsg', error.message)
        })
    },
    isValidDate (d) {
      let date = Date.parse(d)
      return !isNaN(date)
    },
    convertDataToTable (rawBQs) {
      let listHeader = Object.keys(rawBQs[0])
      this.headers = listHeader
      this.bodies = rawBQs
    }
  }
}
</script>

<style lang="sass" scoped>
.title
  color: RGB(192,0,0)
  font-weight: bold
  font-size: 25px !important
  margin: 0
  line-height: 1.3 !important
  margin-bottom: 1px

.subtitle
  font-style: italic
  font-size: 13px
  margin-bottom: 15px
  display: block
</style>
