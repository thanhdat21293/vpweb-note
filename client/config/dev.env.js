'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')
const ENV = process.env

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  SERVER: JSON.stringify(ENV.SERVER || 'http://localhost:3000')
})
