'use strict'
module.exports = {
  NODE_ENV: '"production"',
  SERVER: JSON.stringify(process.env.SERVER || "localhost:3000")
}
