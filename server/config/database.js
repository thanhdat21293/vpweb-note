
const Sequelize = require('sequelize');
const config = require('./config.js')
const sequelize = new Sequelize({
  dialect: 'mssql',
  host: 'localhost',
  dialectModulePath: 'sequelize-msnodesqlv8',
  dialectOptions: {
    trustedConnection: true,
    // instanceName: 'SQLEXPRESS'
  },
  database: config.mssql.database,
  username: null,
  password: null,
})

sequelize
  .authenticate()
  .then(() => {
    console.log('DB Main connect success');
  })
  .catch(err => {
    console.error('DB Main connect fail', err.message);
  });

exports.sequelize = sequelize