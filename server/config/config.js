const ENV = process.env.NODE_ENV || 'dev'

let config = {
  dev: {
    mssql: {
      server: 'THANHDAT', // You can use 'localhost\\instance' to connect to named instance
      database: 'INCENTIVES'
    },
    mssqlOauth: {
      server: 'THANHDAT', // You can use 'localhost\\instance' to connect to named instance
      database: 'INCENTIVES_AUTH'
    },
    secret: 'lwo39s0fel93',
    tableName: {
      incentive: {
        name: 'incentive_201804_macro',
        year: '2018',
        month: '04'
      }
    }
  },
  production: {
    mssql: {
      server: 'THANHDAT', // You can use 'localhost\\instance' to connect to named instance
      database: 'INCENTIVES'
    },
    mssqlOauth: {
      server: 'THANHDAT', // You can use 'localhost\\instance' to connect to named instance
      database: 'INCENTIVES_AUTH'
    },
    secret: 'lwo39s0fel93'
  }
}

module.exports = config[ENV]
