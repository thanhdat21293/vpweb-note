
const Sequelize = require('sequelize');
const config = require('./config.js')
const sequelize = new Sequelize({
  dialect: 'mssql',
  host: 'localhost',
  dialectModulePath: 'sequelize-msnodesqlv8',
  dialectOptions: {
    trustedConnection: true,
    // instanceName: 'SQLEXPRESS'
  },
  database: config.mssqlOauth.database,
  username: null,
  password: null,
})
sequelize
  .authenticate()
  .then(() => {
    console.log('DB Oauth connect success');
  })
  .catch(err => {
    console.error('DB Oauth connect fail:', err.message);
  });

exports.dbOauth = sequelize