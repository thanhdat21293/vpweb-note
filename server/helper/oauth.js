/**
 * Oauth
 */
var jwt = require('jsonwebtoken');
const oauth = require('../helper/oauth.js')
let config = require('../config/config.js')

module.exports = () => {
	var _config = {
		authorization: 'AuthJWT',
		secret: config.secret
	};

  config = config || {};

	if (!config.secret) {
		throw "Secret is empty";
	}
	config = Object.assign(_config, config);

	let decodeJwt = (token) =>{
		return new Promise((resolve, reject) => {
			jwt.verify(token, config.secret, function (err, decoded) {
				if (err) reject(false);
				else resolve(decoded);
			});
		})
	};

	var getToken = function (authorization) {
		if (authorization) {
			authorization = authorization.split(' ');
			if (authorization[0] === config.authorization) {
				return authorization[1];
			} else {
				return null;
			}
		} else {
			return null;
		}
	};

	let setTokenToReq = function (ctx, data) {
		ctx.userId = data.id
		ctx.dao = data.dao
		ctx.token = data.token
		ctx.role = data.role
		ctx.branch_id = data.branch_id
		ctx.position = data.position
		ctx.username = data.username
	};

	return async (ctx, next) => {
		try {
			let headers = ctx['headers'];
			let authenticate = getToken(headers['authorization'])
			if (authenticate) {
				let decoded = await decodeJwt(authenticate)
				if (decoded && decoded.type === 'token') {
					decoded.token = authenticate
					setTokenToReq(ctx, decoded);
					await next();
				} else {
					ctx.body = {
						status: false,
						error: 706,
						msg: "Bạn chưa đăng nhập."
					}
				}
			} else {
				ctx.body = {
					status: false,
					error: 706,
					msg: "Bạn chưa đăng nhập"
				}
			}
		} catch (error) {
			ctx.body = {
				status: false,
				error: 706,
				msg: "Bạn chưa đăng nhập"
			}
		}
	}
};