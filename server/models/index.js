/**
 * Index model
 */

const { sequelize } = require('../config/database.js')
const config = require('../config/config.js')
const databaseName = config.mssql.database

let allTables = () => {
  return sequelize
    .query(`SELECT TABLE_NAME FROM ${databaseName}.INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE'`)
    .then(data => {
      return data[0]
    })
}

exports.allTables = allTables