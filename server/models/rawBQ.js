const { sequelize } = require('../config/database.js')
const typeSelect = sequelize.QueryTypes.SELECT
const tableName = 'rawBQ_201804'

const list = (username, perPage, offset) => {
  return sequelize
    .query(`SELECT * FROM ${tableName} WHERE username = '${username}' ORDER BY cif OFFSET ${offset} ROWS FETCH NEXT ${perPage} ROWS ONLY`, { type: typeSelect})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

const count = (username) => {
  return sequelize
    .query(`SELECT count(app) as count FROM ${tableName} WHERE username = '${username}'`, { type: typeSelect})
    .then(data => {
      return { count: data[0].count, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

const exportDB = (username) => {
  return sequelize
    .query(`SELECT * FROM ${tableName} WHERE username = '${username}'`, { type: typeSelect})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}


exports.list = list
exports.count = count
exports.exportDB = exportDB