const { sequelize } = require('../config/database.js')
const typeSelect = sequelize.QueryTypes.SELECT
const typeInsert = sequelize.QueryTypes.INSERT
const tableName = 'trasoat_insert'

const list = (dao, monthCurrent) => {
  return sequelize
    .query(`SELECT * FROM ${tableName} WHERE dao='${dao}' AND month='${monthCurrent}'`, { type: typeSelect})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

const add = (month, dao, salename, position, item, now) => {
  return sequelize
    .query(`INSERT INTO ${tableName} (month, dao, salename, position, cif, acctno, cardno, app_id_c, reason, detailed_reason, proposal, productname_bmt, email, created_at)
            VALUES('${month}', '${dao}', '${salename}', '${position}', '${item.cif}', '${item.acctno}', '${item.cardno}',
            '${item.app_id_c}', N'${item.reason}', N'${item.detailed_reason}', N'${item.proposal}', N'${item.productname_bmt ? item.productname_bmt.id : ''}', '${item.email}', '${now}')`, { type: typeInsert})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

const exportDB = (dao, monthCurrent) => {
  return sequelize
    .query(`SELECT month, dao, salename, position, cif, acctno, cardno, app_id_c, reason, detailed_reason, proposal, productname_bmt, email FROM ${tableName} WHERE dao='${dao}' AND month='${monthCurrent}'`, { type: typeSelect})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

const del = (id, dao) => {
  return sequelize
    .query(`DELETE FROM ${tableName} WHERE dao='${dao}' AND id='${id}'`, { type: typeSelect})
    .then(data => {
      return { status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

const edit = (id, dao, data) => {
  return sequelize
    .query(`UPDATE ${tableName}
            SET
              cif = '${data.cif}',
              acctno = N'${data.acctno}',
              cardno = N'${data.cardno}',
              app_id_c = N'${data.app_id_c}',
              reason = N'${data.reason}',
              detailed_reason = N'${data.detailed_reason}',
              proposal = N'${data.proposal}',
              productname_bmt = N'${data.productname_bmt}',
              email = '${data.email}'
            WHERE dao='${dao}' AND id='${id}'`, { type: typeSelect})
    .then(data => {
      return { status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

exports.list = list
exports.add = add
exports.exportDB = exportDB
exports.del = del
exports.edit = edit