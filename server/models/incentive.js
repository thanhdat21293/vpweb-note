const { sequelize } = require('../config/database.js')
const typeSelect = sequelize.QueryTypes.SELECT

const list = (dao, tableName) => {
  return sequelize
    .query(`SELECT * FROM ${tableName} WHERE dao = '${dao}' AND date_sale_off is null`, { type: typeSelect})
    .then(data => {
      return data
    })
    .catch(error => {
      return []
    })
}


exports.list = list