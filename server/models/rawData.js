const { sequelize } = require('../config/database.js')
const typeSelect = sequelize.QueryTypes.SELECT
const tableName = 'rawdata_201804_branch'

const list = (username, perPage, offset) => {
  return sequelize
    .query(`SELECT * FROM ${tableName} WHERE username = '${username}' ORDER BY cif OFFSET ${offset} ROWS FETCH NEXT ${perPage} ROWS ONLY`, { type: typeSelect})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      // console.log(error)
      return { status: false }
    })
}

const count = (username) => {
  return sequelize
    .query(`SELECT count(cif) as count FROM ${tableName} WHERE username = '${username}'`, { type: typeSelect})
    .then(data => {
      return { count: data[0].count, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

const filter = (data) => {
  let where = []

  if (data.app.id)
    where.push(`app = '${data.app.id}'`)

  if (data.cif)
    where.push(`cif = '${data.cif}'`)

  if (data.acctno && data.app.id !== 'KH gold')
    where.push(`acctno = '${data.acctno}'`)

  if (data.cardNo && data.app.id === 'The')
    where.push(`schemedesc = '${data.acctno}'`)

  if (data.app_id_c && data.app.id !== 'KH gold')
    where.push(`app_id_c = '${data.app_id_c}'`)
  
  where = where.join(' AND ')
  if(where) {
    where = 'WHERE ' + where
  }
  
  return sequelize
    .query(`SELECT TOP 10 * FROM ${tableName} ${where}`, { type: typeSelect})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

const exportDB = (username) => {
  return sequelize
    .query(`SELECT * FROM ${tableName} WHERE username = '${username}'`, { type: typeSelect})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}


exports.list = list
exports.count = count
exports.filter = filter
exports.exportDB = exportDB