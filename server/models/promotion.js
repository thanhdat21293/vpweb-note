const { sequelize } = require('../config/database.js')
const typeSelect = sequelize.QueryTypes.SELECT
const tableName = 'promotion_201804'

const list = (username, perPage, offset) => {
  return sequelize
    .query(`select dao, staff_id,sale_name,POSITION,branch_id,branch_name,hub,zone_id,city,contract_type,tham_nien,THANGTIEN_STATUS,dieu_chuyen_date,date_sale_start,ranking_5,ranking_5_backdate,ranking_4,ranking_3,ranking_2,ranking_1,ranking_1_backdate,ranking,count_hopdong_vay_co_TSDB,count_hopdong_quahan_vay_co_TSDB,dieukien_chovay_co_TSDB,count_hopdong_vay_UPL_thauchi,count_hopdong_quahan_vay_UPL_thauchi,dieukien_chovay_UPL_thauchi,count_hopdong_the_tindung,count_hopdong_sudung_the_tindung,dieukien_the_tindung,count_hopdong_the_ghino,count_hopdong_sudung_the_ghino,dieukien_the_ghino,note,thang_tien_thuong_vitri,thang_tien_nhanh_vitri from ${tableName} where date_sale_off is null AND username = '${username}' ORDER BY dao OFFSET ${offset} ROWS FETCH NEXT ${perPage} ROWS ONLY`, { type: typeSelect})
    // .query(`SELECT * FROM ${tableName} WHERE username = '${username}' ORDER BY dao OFFSET ${offset} ROWS FETCH NEXT ${perPage} ROWS ONLY`, { type: typeSelect})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}
const count = (username) => {
  return sequelize
    .query(`SELECT count(dao) as count FROM ${tableName} WHERE username = '${username}'`, { type: typeSelect})
    .then(data => {
      return { count: data[0].count, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}


exports.list = list
exports.count = count