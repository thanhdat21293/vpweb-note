const { sequelize } = require('../config/database.js')
const typeSelect = sequelize.QueryTypes.SELECT
const tableName = 'quality_201804'
const tableNameHrBranch = 'a_hr_branch_201804'

const list = (dao, perPage, offset) => {
  return sequelize
    .query(`SELECT * FROM ${tableName} WHERE dao_sp in (select dao from ${tableNameHrBranch} where date_sale_off is null) ORDER BY month OFFSET ${offset} ROWS FETCH NEXT ${perPage} ROWS ONLY`, { type: typeSelect})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

const count = (dao) => {
  return sequelize
    .query(`SELECT count(month) as count FROM ${tableName} WHERE dao_sp in (select dao from ${tableNameHrBranch} where date_sale_off is null)`, { type: typeSelect})
    .then(data => {
      return { count: data[0].count, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}


exports.list = list
exports.count = count