const { dbOauth } = require('../config/oauth.js')
const typeSelect = dbOauth.QueryTypes.SELECT
const tableName = 'users'

const login = (dao, email, password) => {
  return dbOauth
    .query(`SELECT TOP 1 * FROM ${tableName} WHERE dao = '${dao}' AND email = '${email}' AND status = 'active'`, { type: typeSelect})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

const getInfo = (id) => {
  return dbOauth
    .query(`SELECT TOP 1 id, dao, email, role FROM ${tableName} WHERE id = '${id}' AND status = 'active'`, { type: typeSelect})
    .then(data => {
      return { data, status: true }
    })
    .catch(error => {
      return { status: false }
    })
}

exports.login = login
exports.getInfo = getInfo