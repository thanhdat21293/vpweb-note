/**
 * Tra Soat Controller
 */

'use strict';
const { mapLimit } = require("async");
const moment = require('moment')
const ENV = process.env.NODE_ENV || 'dev'
const config = require('../config/config.js')[ENV];
const Controllers = require('./controller.js')
const TraSoatInsertModel = require('../models/traSoatInsert')

class TraSoatInsert extends Controllers {
	constructor(ctx) {
		super(ctx)
  }

  async list () {
    try {
      let data = this.data
      let monthCurrent = moment(new Date()).add(-1, 'M').format('YYYYMM')
      let traSoatInsert = await TraSoatInsertModel.list(this.ctx.dao, monthCurrent)
      
      if (!traSoatInsert.status)
        return this.callback(false, 'Có lỗi xảy ra', [])

      return this.callback(true, 'Tra soat insert list', traSoatInsert.data)
    } catch (error) {
      console.log('error Tra soat insert', error)
      return this.callback(false, 'Có lỗi xảy ra', [])
    }
  }

  async add () {
    try {
      let data = this.data
      let traSoatInsert = await this._addMulti(data)
      
      if (!traSoatInsert.status)
        return this.callback(false, 'Có lỗi xảy ra', [])

      return this.callback(true, 'Thêm thành công', [])
    } catch (error) {
      console.log('error Tra soat insert', error)
      return this.callback(false, 'Có lỗi xảy ra', [])
    }
  }

  _addMulti (data) {
    return new Promise((resolve, reject) => {
      mapLimit(data.duLieuNhap, 1, async (item) => {
        let now = moment(new Date()).format('YYYY-MM-DD H:m:s')
        await TraSoatInsertModel.add(data.month, data.dao, data.salename, data.position, item, now)
      }, (err, results) => {
          if (err) return reject(err.message)
          
          return resolve({ status: true })
      })
    })
  }

  async exportDB () {
    let monthCurrent = moment(new Date()).add(-1, 'M').format('YYYYMM')

    let traSoatInsert = await TraSoatInsertModel.exportDB(this.ctx.dao, monthCurrent)
  
    if (!traSoatInsert.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    // rawBQs.data.map(item => {
    //   item.cif = this._hide2LastNumber(item.cif)
    //   item.customer_name = this._showOnlyName(item.customer_name)
    // })
    let result = {
      data: traSoatInsert.data
    }
    return this.callback(true, 'list tra soat insert', result)

  }

  async del () {
    try {
      let id = this.params.id
      let dao = this.ctx.dao

      if (!id)
        return this.callback(false, 'Dữ liệu k hợp lệ', [])
      if (!dao)
        return this.callback(false, 'Lỗi không tìm thấy dao', [])

      let traSoatInsert = await TraSoatInsertModel.del(id, dao)
  
      if (!traSoatInsert.status)
        return this.callback(false, 'Có lỗi xảy ra', [])

      return this.callback(true, 'Xóa thành công', [])
    } catch (error) {
      console.log('error Tra soat insert DEL', error)
      return this.callback(false, 'Có lỗi xảy ra', [])
    }
  }

  async edit () {
    try {
      let id = this.params.id
      let dao = this.ctx.dao
      let data = this.data

      if (!id)
        return this.callback(false, 'Dữ liệu k hợp lệ', [])

      if (!dao)
        return this.callback(false, 'Lỗi không tìm thấy dao', [])

      let traSoatInsert = await TraSoatInsertModel.edit(id, dao, data)
  
      if (!traSoatInsert.status)
        return this.callback(false, 'Có lỗi xảy ra', [])

      return this.callback(true, 'Sửa thành công', [])
    } catch (error) {
      console.log('error Tra soat insert DEL', error)
      return this.callback(false, 'Có lỗi xảy ra', [])
    }
  }

}

module.exports = TraSoatInsert;