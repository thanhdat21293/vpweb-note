/**
 * Raw BQ Controller
 */

'use strict';
const ENV = process.env.NODE_ENV || 'dev'
const config = require('../config/config.js')[ENV];
const Controllers = require('./controller.js')
const RawBQModel = require('../models/rawBQ')

class RawBQ extends Controllers {
	constructor(ctx) {
		super(ctx)
  }

  async list () {
    let data = this.data
    let currentPage = data.currentPage ? parseInt(data.currentPage) : 1
    let perPage = 10
    let offset = 0
    
    if (currentPage) {
      offset = (currentPage - 1) * perPage
    }

    let rawBQs = await RawBQModel.list(this.ctx.username, perPage, offset)
    let count = await RawBQModel.count(this.ctx.username)
  
    if (!rawBQs.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    if (!count.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    rawBQs.data.map(item => {
      item.cif = this._hide2LastNumber(item.cif)
      item.customer_name = this._showOnlyName(item.customer_name)
    })
    let result = {
      data: rawBQs.data,
      total: count.count
    }
    return this.callback(true, 'list raw BQ', result)

  }

  async exportDB () {

    let rawBQs = await RawBQModel.exportDB(this.ctx.username)
  
    if (!rawBQs.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    rawBQs.data.map(item => {
      item.cif = this._hide2LastNumber(item.cif)
      item.customer_name = this._showOnlyName(item.customer_name)
    })
    let result = {
      data: rawBQs.data
    }
    return this.callback(true, 'list raw BQ', result)

  }
}

module.exports = RawBQ;