/**
 * User Controller
 */

'use strict';
const moment = require('moment')
const { mapLimit } = require("async");
const config = require('../config/config.js');
const Controllers = require('./controller.js')
const IncentiveModel = require('../models/incentive')
const IndexModel = require('../models/index')

const monthOfYear = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
const yearMonthCurrent = config.tableName.incentive.year + '-' + config.tableName.incentive.month
const tableNameCurrent = config.tableName.incentive.name

class Incentives extends Controllers {
	constructor(ctx) {
		super(ctx)
  }

  async list () {
    try {
      let month = this.data.month || ''
      // let monthCurrent = moment(new Date).format('M')
      // let yearCurrent = moment(new Date).format('YYYY')
      // let listMonths = []
      // for (let i = monthCurrent - 1; i >= 0; i--) {
      //   listMonths.push(monthOfYear[i])
      // }

      // let getAllTables = await IndexModel.allTables()
      // let allTables = []
      // getAllTables.map(item => {
      //   allTables.push(item.TABLE_NAME)
      // })

      // let listMonthsExists = []
      // listMonths.map(item => {
      //   let tableName = 'incentive_' + yearCurrent + item
      //   if (allTables.indexOf(tableName) >= 0) {
      //     listMonthsExists.push(item)
      //   }
      // })

      let tableName = tableNameCurrent
      let yearMonth = yearMonthCurrent

      if (month) {
        let month1 = moment(month).format('YYYYMM')
        tableName = 'incentive_' + month1 + '_macro'
        yearMonth = month
      }

      let data = await IncentiveModel.list(this.ctx.dao, tableName)
      await this._timeOut(3000)
      return this.callback(true, 'list incentive', { data, date: yearMonth })
    } catch (error) {
      console.log('error Incentive', error)
      return this.callback(false, 'Có lỗi xảy ra', [])
    }
  }

  _timeOut(minisecond) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(true)
      }, minisecond)
    })
  }

  // _getDataByListMonths (listMonthsExists) {
  //   return new Promise((resolve, reject) => {
  //     let yearCurrent = moment(new Date).format('YYYY')
  //     mapLimit(listMonthsExists, 1, async (item) => {
  //       let tableName = 'incentive_' + yearCurrent + item + '_macro'
  //       let result = await IncentiveModel.list(this.ctx.dao, tableName)
  //       return {
  //         date: yearCurrent + '-' + item,
  //         data: result
  //       }
  //     }, (err, results) => {
  //         if (err) return reject(err.message)
  //         // results is now an array of the response bodies
  //         return resolve(results)
  //     })
  //   })
  // }

  // async getInfo() {
  //   try {
  //     let monthCurrent = moment(new Date).format('M')
  //     let yearCurrent = moment(new Date).format('YYYY')
  //     let listMonths = []
  //     for (let i = monthCurrent - 1; i >= 0; i--) {
  //       listMonths.push(monthOfYear[i])
  //     }

  //     let getAllTables = await IndexModel.allTables()
  //     let allTables = []
  //     getAllTables.map(item => {
  //       allTables.push(item.TABLE_NAME)
  //     })

  //     let tableCurrent = false

  //     listMonths.map(item => {
  //       let tableName = 'incentive_' + yearCurrent + item
  //       if (allTables.indexOf(tableName) >= 0 && !tableCurrent) {
  //         tableCurrent = tableName
  //       }
  //     })

  //     console.log(listMonthsExists)

  //     let data = await this._getDataByListMonths(listMonthsExists)

  //     return this.callback(true, 'list incentive', data)
  //   } catch (error) {
  //     console.log('error Incentive', error)
  //     return this.callback(false, 'Có lỗi xảy ra', [])
  //   }
  // }
}

module.exports = Incentives;