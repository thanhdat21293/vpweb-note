/**
 * Controllers
 */

'use strict';
const ENV = process.env.NODE_ENV || 'dev'
const config = require('../config/config.js')[ENV];
// const request = require('request');

class Controllers {
	constructor(ctx) {
		this.ctx = ctx
    this.data = ctx.request.body || ctx.query;
		this.params = ctx.params;

		this.config = config;
		
		this.userId = ctx.userId
		this.token = ctx.token
  }
  
  getIncentivesByDao (daoId) {
		this.ctx.body = 1
	}

	callback (status, msg, data) {
		return new Promise ((resolve, reject) => {
			resolve ({
				status,
				msg,
				data
			})
		})
	}

  _showOnlyName (fullName) {
    let arr = fullName.split(' ')
    arr = arr.filter((n) => { return n.length > 0 })
    let count = arr.length
    for (let i = 0; i < count; i++) {
      if (i < count - 1) {
        arr[i] = '-'
      }
    }
    let onlyName = arr.join(' ')
    return onlyName
  }

  _hide2LastNumber (number) {
    let arr = number.split('')
    let count = arr.length
    arr[count - 1] = 'x'
    arr[count - 2] = 'x'
    let numberHide = arr.join('')
    return numberHide
  }
}

module.exports = Controllers;