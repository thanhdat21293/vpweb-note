/**
 * Tra Soat Controller
 */

'use strict';
const ENV = process.env.NODE_ENV || 'dev'
const config = require('../config/config.js')[ENV];
const Controllers = require('./controller.js')
const TraSoatModel = require('../models/traSoat')

class TraSoat extends Controllers {
	constructor(ctx) {
		super(ctx)
  }

  async getOne () {
    let traSoat = await TraSoatModel.getOne(this.ctx.dao)
    
    if (!traSoat.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    return this.callback(true, 'tra soat', traSoat.data)
  }

  // table incentive
  // _getTableNameLastMonth () {
  //   return new Promise((resolve, reject) => {
  //     let yearCurrent = moment(new Date).format('YYYY')
  //     mapLimit(listMonthsExists, 1, async (item) => {
  //       let tableName = 'incentive_' + yearCurrent + item
  //       let result = await IncentiveModel.list(this.ctx.dao, tableName)
  //       return {
  //         date: yearCurrent + '-' + item,
  //         data: result
  //       }
  //     }, (err, results) => {
  //         if (err) return reject(err.message)
  //         // results is now an array of the response bodies
  //         return resolve(results)
  //     })
  //   })
  // }

}

module.exports = TraSoat;