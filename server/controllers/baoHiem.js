/**
 * Bao Hiem Controller
 */

'use strict';
const ENV = process.env.NODE_ENV || 'dev'
const config = require('../config/config.js')[ENV];
const Controllers = require('./controller.js')
const BaoHiemModel = require('../models/baoHiem')

class BaoHiem extends Controllers {
	constructor(ctx) {
		super(ctx)
  }

  async list () {
    let data = this.data
    let currentPage = data.currentPage ? parseInt(data.currentPage) : 1
    let perPage = 10
    let offset = 0
    
    if (currentPage) {
      offset = (currentPage - 1) * perPage
    }

    let baoHiem = await BaoHiemModel.list(this.ctx.username, perPage, offset)
    let count = await BaoHiemModel.count(this.ctx.username)
  
    if (!baoHiem.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    if (!count.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    baoHiem.data.map(item => {
      item.CIF = this._hide2LastNumber(item.CIF)
      item.CUSTOMER_NAME = this._showOnlyName(item.CUSTOMER_NAME)
    })

    let result = {
      data: baoHiem.data,
      total: count.count
    }
    return this.callback(true, 'list bao hiem', result)

  }
}

module.exports = BaoHiem;