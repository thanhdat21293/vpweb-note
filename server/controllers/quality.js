/**
 * Quality Controller
 */

'use strict';
const ENV = process.env.NODE_ENV || 'dev'
const config = require('../config/config.js')[ENV];
const Controllers = require('./controller.js')
const QualityModel = require('../models/quality')

class Quality extends Controllers {
	constructor(ctx) {
		super(ctx)
  }

  async list () {
    let data = this.data
    let currentPage = data.currentPage ? parseInt(data.currentPage) : 1
    let perPage = 10
    let offset = 0
    
    if (currentPage) {
      offset = (currentPage - 1) * perPage
    }
    let quality = await QualityModel.list(this.ctx.dao, perPage, offset)
    let count = await QualityModel.count(this.ctx.dao)
    
    if (!quality.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    if (!count.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    quality.data.map(item => {
      item.CIF = this._hide2LastNumber(item.CIF)
      item.CUSTOMER_NAME = this._showOnlyName(item.CUSTOMER_NAME)
    })

    let result = {
      data: quality.data,
      total: count.count
    }

    return this.callback(true, 'list quality', result)

  }
}

module.exports = Quality;