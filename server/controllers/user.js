/**
 * User Controller
 */

'use strict';
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')
const moment = require('moment')

const ENV = process.env.NODE_ENV || 'dev'
const config = require('../config/config.js')[ENV];
const Controllers = require('./controller.js')
const UserModel = require('../models/user')
const IncentiveModel = require('../models/incentive')
const IndexModel = require('../models/index')

const monthOfYear = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']

const round = 10

class Users extends Controllers {
	constructor(ctx) {
		super(ctx)
  }
  
  async login () {
    let data = this.data
    let dao = data.dao
    let email = data.email
    let password = data.password
    
    if (!dao)
      return this.callback(false, 'Dao không được bỏ trống', [])
    
    if (!email)
      return this.callback(false, 'Email không được bỏ trống', [])
    
    if (!password)
      return this.callback(false, 'Password không được bỏ trống', [])
    
    if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email))
      return this.callback(false, 'Email không hợp lệ', [])
    
    let users = await UserModel.login(dao, email)
    // console.log(users)
    if (!users.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    if (users.data && users.data.length === 0)
      return this.callback(false, 'Tài khoản không hợp lệ', [])

    let userCurrent = users.data[0]
    let userPassword = userCurrent.password
    let checkPassword = bcrypt.compareSync(password, userPassword)

    if (!checkPassword)
      return this.callback(false, 'Mật khẩu không đúng', [])

    
    let monthCurrent = moment(new Date).format('M')
    let yearCurrent = moment(new Date).format('YYYY')
    let listMonths = []
    for (let i = monthCurrent - 1; i >= 0; i--) {
      listMonths.push(monthOfYear[i])
    }

    let getAllTables = await IndexModel.allTables()
    let allTables = []
    getAllTables.map(item => {
      allTables.push(item.TABLE_NAME)
    })

    let tableCurrent = false

    listMonths.map(item => {
      let tableName = 'incentive_' + yearCurrent + item
      if (allTables.indexOf(tableName) >= 0 && !tableCurrent) {
        tableCurrent = tableName
      }
    })
    
    let result = await IncentiveModel.list(userCurrent.dao, tableCurrent)
    let branch_id = ''
    let position = ''
    let username = ''
    if (result.length > 0) {
      branch_id = result[0].branch_id
      position = result[0]['VỊ TRÍ']
      username = result[0].username
    }

    let payload = {
      id: userCurrent.id,
      dao: userCurrent.dao.trim(),
      role: userCurrent.role,
      status: userCurrent.status,
      branch_id: branch_id,
      position: position,
      username: username,
      type: 'token'
    }

    let token = jwt.sign(payload, config.secret, { expiresIn: '7d' })

    return this.callback(true, 'Thành công', { token: token })
  }

  async getInfo () {
    let users = await UserModel.getInfo(this.ctx.userId)
    
    if (!users.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    if (users.data.length === 0)
      return this.callback(false, 'Người dùng không tồn tại', [])

      return this.callback(true, 'Thông tin người dùng', users.data[0])

  }
}

module.exports = Users;