/**
 * Promotion Controller
 */

'use strict';
const ENV = process.env.NODE_ENV || 'dev'
const config = require('../config/config.js')[ENV];
const Controllers = require('./controller.js')
const PromotionModel = require('../models/promotion')

class Promotion extends Controllers {
	constructor(ctx) {
		super(ctx)
  }

  async list () {
    let data = this.data
    let currentPage = data.currentPage ? parseInt(data.currentPage) : 1
    let perPage = 10
    let offset = 0
    
    if (currentPage) {
      offset = (currentPage - 1) * perPage
    }

    let promotion = await PromotionModel.list(this.ctx.username, perPage, offset)
    let count = await PromotionModel.count(this.ctx.username)
  
    if (!promotion.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    if (!count.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    let result = {
      data: promotion.data,
      total: count.count
    }
    return this.callback(true, 'list promotion', result)

  }
}

module.exports = Promotion;