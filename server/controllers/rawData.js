/**
 * Raw Data Controller
 */

'use strict';
const moment = require('moment')
const ENV = process.env.NODE_ENV || 'dev'
const config = require('../config/config.js')[ENV];
const Controllers = require('./controller.js')
const RawDataModel = require('../models/rawData')

class RawData extends Controllers {
	constructor(ctx) {
		super(ctx)
  }

  async list () {
    let data = this.data
    let currentPage = data.currentPage ? parseInt(data.currentPage) : 1
    let perPage = 10
    let offset = 0
    
    if (currentPage) {
      offset = (currentPage - 1) * perPage
    }
    let rawDatas = await RawDataModel.list(this.ctx.username, perPage, offset)
    let count = await RawDataModel.count(this.ctx.username)
    
    if (!rawDatas.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    if (!count.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    rawDatas.data.map(item => {
      item.cif = this._hide2LastNumber(item.cif)
      item.customer_name = this._showOnlyName(item.customer_name)
    })

    let result = {
      data: rawDatas.data,
      total: count.count
    }

    return this.callback(true, 'list raw data', result)
  }

  async filter () {
    let data = this.data
    if (!data.app || !data.app.id)
      return this.callback(false, 'Bạn phải chọn sản phẩm theo tra soát', [])

    if (!data.cif)
      return this.callback(false, 'Cif là trường bắt buộc', [])

    if (!data.acctno && data.app.id === 'KH gold')
      return this.callback(false, 'Acctno là trường bắt buộc', [])

    let rawDatas = await RawDataModel.filter(data)
    
    if (!rawDatas.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

    return this.callback(true, 'filter raw data', rawDatas.data)
  }

  async exportDB () {
    let rawDatas = await RawDataModel.exportDB(this.ctx.username)
  
    if (!rawDatas.status)
      return this.callback(false, 'Có lỗi xảy ra', [])

      rawDatas.data.map(item => {
      item.cif = this._hide2LastNumber(item.cif)
      item.customer_name = this._showOnlyName(item.customer_name)
    })
    let result = {
      data: rawDatas.data
    }
    return this.callback(true, 'list raw data', result)

  }
}

module.exports = RawData;