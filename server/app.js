const Koa = require('koa');
const Router = require('koa-router');
const cors = require('koa-cors');
const app = new Koa();
const bodyParser = require('koa-bodyparser');
const logger = require('koa-logger')
const shortid = require('shortid')
const router = Router();

require('./config/database.js')
require('./config/oauth.js')

// if(process.env.NODE_ENV === 'production') {
// 	 router = Router({prefix: '/api'});
// }else {
// 	 router = Router();
// }

app.use(cors())
app.use(logger())
app.use(bodyParser());

// app.use(async (ctx, next) => {
//   ctx.set("Access-Control-Allow-Origin", "*");
// 	ctx.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Content-Disposition, Authorization");
// 	ctx.set("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
// 	ctx.set("Access-Control-Allow-Credentials", true);
// 	await next()
// });

router.get('/test', async ctx => {
  ctx.body = 'ok'
})
require('./routes/router.js')(router)


// app.use(ctx => {
// 	ctx.body = {
// 		msg: 'error',
// 		status: false
// 	}
// })

app.use(router.routes())
app.use(router.allowedMethods())

const port = 3000
app.listen(port, () => {
  console.log('Start http://localhost:' + port)
});
