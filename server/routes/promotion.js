/**
 * Promotion route
 */
'use strict';

const promotionColltroller = require('../controllers/promotion');
const oauth = require('../helper/oauth.js')

const list = async (ctx) => {
	let promotion = new promotionColltroller(ctx);
	let data = await promotion.list()
	ctx.body = data
};

module.exports = (router) => {
  router.post('/api/promotion/list', oauth(), list);
}