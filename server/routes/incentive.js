/**
 * User route
 */
'use strict';

const incentiveColltroller = require('../controllers/incentive');
const oauth = require('../helper/oauth.js')

const list = async (ctx) => {
	let Incentive = new incentiveColltroller(ctx);
	let data = await Incentive.list()
	ctx.body = data
};

module.exports = (router) => {
  router.post('/api/incentive/list', oauth(), list);
}