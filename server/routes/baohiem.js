/**
 * Bao hiem route
 */
'use strict';

const baoHiemColltroller = require('../controllers/baoHiem');
const oauth = require('../helper/oauth.js')

const list = async (ctx) => {
	let baoHiem = new baoHiemColltroller(ctx);
	let data = await baoHiem.list()
	ctx.body = data
};

module.exports = (router) => {
  router.post('/api/bao-hiem/list', oauth(), list);
}