/**
 * Tra soat route
 */
'use strict';

const traSoatColltroller = require('../controllers/traSoat');
const oauth = require('../helper/oauth.js')

const getOne = async (ctx) => {
	let traSoat = new traSoatColltroller(ctx);
	let data = await traSoat.getOne()
	ctx.body = data
};

module.exports = (router) => {
  router.post('/api/tra-soat/one', oauth(), getOne);
}