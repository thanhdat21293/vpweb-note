/**
 * Tra soat route
 */
'use strict';

const traSoatInsertColltroller = require('../controllers/traSoatInsert');
const oauth = require('../helper/oauth.js')

const list = async (ctx) => {
	let traSoatInsert = new traSoatInsertColltroller(ctx);
	let data = await traSoatInsert.list()
	ctx.body = data
};

const add = async (ctx) => {
	let traSoatInsert = new traSoatInsertColltroller(ctx);
	let data = await traSoatInsert.add()
	ctx.body = data
};

const edit = async (ctx) => {
	let traSoatInsert = new traSoatInsertColltroller(ctx);
	let data = await traSoatInsert.edit()
	ctx.body = data
};

const del = async (ctx) => {
	let traSoatInsert = new traSoatInsertColltroller(ctx);
	let data = await traSoatInsert.del()
	ctx.body = data
};

const exportDB = async (ctx) => {
	let traSoatInsert = new traSoatInsertColltroller(ctx);
	let data = await traSoatInsert.exportDB()
	ctx.body = data
};

module.exports = (router) => {
  router.post('/api/tra-soat-insert/list', oauth(), list);
  router.post('/api/tra-soat-insert/add', oauth(), add);
  router.post('/api/tra-soat-insert/edit/:id', oauth(), edit);
  router.post('/api/tra-soat-insert/del/:id', oauth(), del);
  router.post('/api/tra-soat-insert/exports', oauth(), exportDB);
}