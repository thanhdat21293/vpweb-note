/**
 * User route
 */
'use strict';

const usersColltroller = require('../controllers/user');
const oauth = require('../helper/oauth.js')

const login = async (ctx) => {
	let Users = new usersColltroller(ctx);
	let data = await Users.login()
	ctx.body = data
};

const getInfo = async (ctx) => {
	let Users = new usersColltroller(ctx);
	let data = await Users.getInfo()
	ctx.body = data
};

module.exports = (router) => {
  router.post('/api/user/login', login);
  router.post('/api/user/info', oauth(), getInfo);
}