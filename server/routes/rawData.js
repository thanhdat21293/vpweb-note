/**
 * Raw Data route
 */
'use strict';

const rawDataColltroller = require('../controllers/rawData');
const oauth = require('../helper/oauth.js')

const list = async (ctx) => {
	let rawData = new rawDataColltroller(ctx);
	let data = await rawData.list()
	ctx.body = data
};

const filter = async (ctx) => {
	let rawData = new rawDataColltroller(ctx);
	let data = await rawData.filter()
	ctx.body = data
};

const exportDB = async (ctx) => {
	let rawData = new rawDataColltroller(ctx);
	let data = await rawData.exportDB()
	ctx.body = data
};

module.exports = (router) => {
  router.post('/api/raw-data/list', oauth(), list);
  router.post('/api/raw-data/filter', oauth(), filter);
  router.post('/api/raw-data/exports', oauth(), exportDB);
}