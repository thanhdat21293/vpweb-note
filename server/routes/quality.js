/**
 * Quality route
 */
'use strict';

const qualityColltroller = require('../controllers/quality');
const oauth = require('../helper/oauth.js')

const list = async (ctx) => {
	let quality = new qualityColltroller(ctx);
	let data = await quality.list()
	ctx.body = data
};

module.exports = (router) => {
  router.post('/api/quality/list', oauth(), list);
}