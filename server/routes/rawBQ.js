/**
 * Raw BQ route
 */
'use strict';

const rawBQColltroller = require('../controllers/rawBQ');
const oauth = require('../helper/oauth.js')

const list = async (ctx) => {
	let rawBQ = new rawBQColltroller(ctx);
	let data = await rawBQ.list()
	ctx.body = data
};

const exportDB = async (ctx) => {
	let rawBQ = new rawBQColltroller(ctx);
	let data = await rawBQ.exportDB()
	ctx.body = data
};

module.exports = (router) => {
  router.post('/api/raw-bq/list', oauth(), list);
  router.post('/api/raw-bq/exports', oauth(), exportDB);
}