/**
 * Routes
 * @param router
 */
// const Controllers = require('../controllers/controllers.js')
// const oauth = require('../helper/oauth.js')

module.exports = (router) => {

  // router.get('/incentives', oauth(), async ctx => {
  //   await new Controllers(ctx).getIncentivesByDao()
  // })

  // router.get('/test', async ctx => {
  //   await new Controllers(ctx).getIncentivesByDao()
  // })

  require('./user.js')(router)
  require('./incentive.js')(router)
  require('./rawData.js')(router)
  require('./rawBQ.js')(router)
  require('./quality.js')(router)
  require('./trasoat.js')(router)
  require('./traSoatInsert.js')(router)
  require('./promotion.js')(router)
  require('./baoHiem.js')(router)

  return router
}
